const webpack = require('webpack');
const WebpackNotifierPlugin = require('webpack-notifier');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const helpers = require('./helpers');

module.exports = {
	entry: {
		'polyfills': './src/polyfills.ts',
		'vendor': './src/vendor.ts',
		'app': './src/main.ts'
	},
	resolve: {
		extensions: ['.ts', '.js']
	},
	module: {
		rules: [
            //tslint loader
            {
                test: /\.(ts|tsx)$/,
                enforce: 'pre',
                loader: 'tslint-loader',
                options: {
                    failOnHint: true,
                    configuration: require('../tslint.json')
                }
            },
			//typescript loader
			{
				test: /\.ts$/,
				use: [
					{
						loader: 'awesome-typescript-loader?useBabel=true&useWebpackText=true',
                        options: {
                            configFileName: helpers.root('tsconfig.json')
                        }
					},
					{
						loader: 'angular2-template-loader'
					}
				]
			},
			// html loader
			{
				test: /\.html$/,
				loader: 'raw-loader',
				exclude: [helpers.root('src/index.html')]
			},
			// static assets
			{
				test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
				loader: 'file-loader?name=assets/[name].[hash].[ext]'
			},
			// css global which not include in components
			{
				test: /\.css$/,
				exclude: helpers.root('src', 'app'),
				use: ExtractTextPlugin.extract({
					use: "raw-loader"
				})
			},
			// css loader and inject into components
			{
				test: /\.css$/,
				include: helpers.root('src', 'app'),
				loader: 'raw-loader'
			},
			// SASS loader and inject into components      
			{
				test: /\.scss$/,
				include: helpers.root('src', 'app'),
				use: ['raw-loader', 'sass-loader']
			},
			// SASS global which not include in components
			{
				test: /\.scss$/,
				exclude: helpers.root('src', 'app'),
				use: ExtractTextPlugin.extract({
					use: ['raw-loader', 'sass-loader']
				})

			}
		]
	},
	plugins: [
        new WebpackNotifierPlugin(),
        new webpack.NamedModulesPlugin(),
		new webpack.optimize.CommonsChunkPlugin({
			name: ['app', 'vendor', 'polyfills']
		}),
		new HtmlWebpackPlugin({
			template: 'src/index.html'
		}),
		new CopyWebpackPlugin([
			{ from: 'src/assets', to: 'assets' }
		]),
		new webpack.ProvidePlugin({
			jQuery: 'jquery',
			$: 'jquery',
			jquery: 'jquery'
		}),
        new webpack.ContextReplacementPlugin(
            // The (\\|\/) piece accounts for path separators in *nix and Windows
            /angular(\\|\/)core(\\|\/)@angular/,
            helpers.root('./src'), // location of your src
            {} // a map of your routes
        ),
	]
};
