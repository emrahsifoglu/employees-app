import { Directive } from '@angular/core';
import { NG_VALIDATORS, FormControl, Validator, ValidationErrors } from '@angular/forms';
import * as moment from 'moment';

@Directive({
    selector: '[date]',
    providers: [{ provide: NG_VALIDATORS, useExisting: DateValidatorDirective, multi: true }]
})
export class DateValidatorDirective implements Validator {

    validate(c: FormControl): ValidationErrors {
        const value = moment(new Date(c.value)).format('DD.MM.YYYY');
        const isDateValid = /^[0-3]?[0-9].[0-3]?[0-9].(?:[0-9]{2})?[0-9]{2}$/.test(value);
        const message = {
            'format': {
                'message': 'The date is not valid'
            }
        };
        return isDateValid ? null : message;
    }
}
