import {
    Directive,
    ElementRef,
    Input,
    Output,
    OnChanges,
    EventEmitter,
    HostListener,
    SimpleChanges
} from '@angular/core';

@Directive({
    selector: '[contentEditableModel]'
})
export class ContentEditableModelDirective implements OnChanges {

    @Input('contentEditableModel')
    public model: any;

    @Output('contentEditableModelChange')
    public update = new EventEmitter();

    private _lastViewModel: any;

    constructor(private elRef: ElementRef) {}

    public ngOnChanges(changes: SimpleChanges): void {
        if (this._lastViewModel !== changes['model'].currentValue) {
            this._lastViewModel = this.model;
            this._refreshView();
        }
    }

    @HostListener('blur')
    public onBlur() {
        let value = this.elRef.nativeElement.innerText;
        this._lastViewModel = value;
        this.update.emit(value);
    }

    private _refreshView() {
        this.elRef.nativeElement.innerText = this.model;
    }
}
