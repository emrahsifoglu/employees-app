/**
 * Server Mutation Query
 */

'use strict';

import gql from 'graphql-tag';

export const addEmployee = gql`
  mutation addEmployee($name: String!, $surname: String!, $dateOfBirth: String!, $jobTitle: String!) {
    addEmployee(name: $name, surname: $surname, dateOfBirth: $dateOfBirth, jobTitle: $jobTitle) {
      _id
      name
      surname
      dateOfBirth
      jobTitle
    }
  }`;

export const Employees = gql`
  query {
    employees {
      _id
      name
      surname
      dateOfBirth
      jobTitle
    }
  }`;

export const removeEmployee = gql`
  mutation removeEmployee($_id: ID!) {
    removeEmployee(_id: $_id) {
      _id
      name
      surname
      dateOfBirth
      jobTitle
    }
  }`;

export const updateEmployee = gql`
  mutation updateEmployee($_id: ID!, $name: String!, $surname: String!, $dateOfBirth: String!, $jobTitle: String!) {
    updateEmployee(_id: $_id, name: $name, surname: $surname, dateOfBirth: $dateOfBirth, jobTitle: $jobTitle) {
      _id
      name
      surname
      dateOfBirth
      jobTitle
    }
  }`;
