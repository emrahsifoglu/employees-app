import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";

export interface Employee {
    _id: string;
    __typename: string;
    name: string;
    surname: string;
    dateOfBirth: string;
    jobTitle: string;
}
@Injectable()
export abstract class EmployeeService {
    abstract getEmployees(): Observable<Employee[]>;
    abstract createEmployee(employee: Employee): any;
    abstract updateEmployee(employee: Employee): any;
    abstract removeEmployee(employee: Employee): any;
}
