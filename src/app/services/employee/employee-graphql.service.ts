import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import * as Query from './employee-graphql.query';
import { Employee, EmployeeService } from './employee.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class EmployeeGraphQLService implements EmployeeService {

    constructor(
        private apollo: Apollo
    ) {

    }

    public getEmployees(): Observable<Employee[]> {
        return this.apollo.watchQuery({ query: Query.Employees })
            .valueChanges
            .map((result: any) => result.data.employees.map((employee: Employee) => <Employee>(employee) ));
    }

    /**
     * Create Employee
     * @param {Employee} employee
     */
    public createEmployee(employee: Employee) {
        return this.apollo
            .mutate({
                mutation: Query.addEmployee,
                variables: {
                    name: employee.name,
                    surname: employee.surname,
                    dateOfBirth: employee.dateOfBirth,
                    jobTitle: employee.jobTitle
                },
                update: (proxy, { data: { addEmployee } }) => {
                    // Read the data from our cache for this query.
                    const data: any = proxy.readQuery({ query: Query.Employees });

                    data.employees.push(addEmployee);

                    // Write our data back to the cache.
                    proxy.writeQuery({ query: Query.Employees, data });
                }
            });
    }

    /**
     * Update Employee
     * @param {Employee} employee
     */
    public updateEmployee(employee: Employee) {
        return this.apollo
            .mutate({
                mutation: Query.updateEmployee,
                variables: {
                    _id: employee._id,
                    name: employee.name,
                    surname: employee.surname,
                    dateOfBirth: employee.dateOfBirth,
                    jobTitle: employee.jobTitle
                },
                update: (proxy, { data: { updateEmployee } }) => {
                    // Read the data from our cache for this query.
                    const data: any = proxy.readQuery({ query: Query.Employees });

                    let index = data.employees.map(function (x: any) { return x._id; }).indexOf(employee._id);

                    data.employees[index].name = employee.name;
                    data.employees[index].surname = employee.surname;
                    data.employees[index].dateOfBirth = employee.dateOfBirth;
                    data.employees[index].jobTitle = employee.jobTitle;

                    // Write our data back to the cache.
                    proxy.writeQuery({ query: Query.Employees, data });
                }
            });
    }

    /**
     * Remove Employee
     * @param {Employee} employee
     */
    public removeEmployee(employee: Employee) {
        return this.apollo
            .mutate({
                mutation: Query.removeEmployee,
                variables: {
                    _id: employee._id
                },
                update: (proxy, { data: { removeEmployee } }) => {
                    // Read the data from our cache for this query.
                    const data: any = proxy.readQuery({ query: Query.Employees });

                    let index = data.employees.map(function (x: any) { return x._id; }).indexOf(employee._id);

                    data.employees.splice(index, 1);

                    // Write our data back to the cache.
                    proxy.writeQuery({ query: Query.Employees, data });
                }
            });
    }

}
