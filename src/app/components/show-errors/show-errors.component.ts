import { Component, Input } from '@angular/core';
import { AbstractControlDirective, AbstractControl } from '@angular/forms';

@Component({
    selector: 'show-errors',
    templateUrl: './show-errors.component.html',
    styleUrls: ['./show-errors.component.css']
})
export class ShowErrorsComponent {

    private static readonly errorMessages = {
        'required': () => 'This field is required',
        'minlength': (params: any) => 'The min number of characters is ' + params.requiredLength,
        'maxlength': (params: any) => 'The max allowed number of characters is ' + params.requiredLength,
        'pattern': (params: any) => 'The required pattern is: ' + params.requiredPattern,
        'format': (params: any) => params.message // this is need because of date picker
    };

    @Input()
    private control: AbstractControlDirective | AbstractControl;

    public shouldShowErrors(): boolean {
        return this.control && this.control.errors && (this.control.dirty || this.control.touched);
    }

    public listOfErrors(): string[] {
        return Object.keys(this.control.errors)
            .map(field => this.getMessage(field, this.control.errors[field]));
    }

    private getMessage(type: string, params: any) {
        // todo check not defined error types
        return ShowErrorsComponent.errorMessages[type](params);
    }

}
