import { Component, TemplateRef, OnInit } from '@angular/core';
import { FormGroup, FormControl, NgForm } from '@angular/forms';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { AlertService } from '../../services/alert.service';
import { Employee } from '../../services/employee/employee.service';
import { EmployeeGraphQLService } from '../../services/employee/employee-graphql.service';
import * as moment from 'moment';
import 'rxjs/add/operator/map';

@Component({
    selector: 'employees-list',
    templateUrl: './employees-list.component.html',
    styleUrls: ['./employees-list.component.css']
})
export class EmployeesListComponent implements OnInit {

    private employeeForm: FormGroup;

    modalRef: BsModalRef;

    employee: Employee;
    employees: Array<Employee> = [];

    datepickerOpts = {
        autoclose: true,
        format: 'dd.mm.yyyy'
    };

    /**
     * @param {AlertService} alertService
     * @param {EmployeeService} employeeService
     * @param {BsModalService} modalService
     */
    constructor(private alertService: AlertService,
                private employeeService: EmployeeGraphQLService,
                private modalService: BsModalService) {
    }

    ngOnInit() {
        this.createForm();
    }

    private createForm() {
        this.employeeForm = new FormGroup({
            _id: new FormControl(''),
            name: new FormControl(''),
            surname: new FormControl(''),
            dateOfBirth: new FormControl(''),
            jobTitle: new FormControl(''),
        });
    }

    /**
     * @return Employee[]
     */
    public getEmployees(): any {
        this.employeeService.getEmployees()
            .subscribe((data: Employee[]) => {
                this.employees = data;
            });
    }

    /**
     * @param {Employee} employee
     */
    private createEmployee(employee: Employee) {
        this.employeeService.createEmployee(employee)
            .subscribe((data) => {
                this.closeFirstModal();
                console.log(data);
            }, (error) => {
                console.log('there was an error on creating employee', error);
            });
    }

    /**
     * @param {Employee} employee
     */
    private updateEmployee(employee: Employee) {
        this.employeeService.updateEmployee(employee)
            .subscribe((data) => {
                this.closeFirstModal();
                console.log(data);
            }, (error) => {
                console.log('there was an error on updating employee', error);
            });
    }

    /**
     * @param {Employee} employee
     */
    public removeEmployee(employee: Employee) {
        let that = this;
        this.alertService.confirmThis('You sure Bro?', function() {
            that.employeeService.removeEmployee(employee)
                .subscribe((data) => {
                    console.log(data);
                }, (error) => {
                    console.log('there was an error on removing employee', error);
                });
        }, function () {

        });
    }

    /**
     * @param {string} value
     * @param {string} property
     * @param {number} index
     */
    public saveEditable(value: string, property: string, index: number) {
        let employee: Employee = <Employee>(this.employees[index]);

        let _id: string = employee._id;
        let name: string = employee.name;
        let surname: string = employee.surname;
        let dateOfBirth: string = employee.dateOfBirth;
        let jobTitle: string = employee.jobTitle;

        // todo set object value by key
        switch (property) {
            case 'name': name = value;
                break;
            case 'surname': surname = value;
                break;
            case 'dateOfBirth': dateOfBirth = value;
                break;
            case 'jobTitle': jobTitle = value;
                break;
        }

        employee = <Employee>({
            _id: _id,
            name: name,
            surname: surname,
            dateOfBirth: dateOfBirth,
            jobTitle: jobTitle,
        });

        this.employeeService.updateEmployee(employee)
            .subscribe((data) => {
                console.log(data);
            }, (error) => {
                console.log('there was an error on updating ' + property, error);
            });
    }

    /**
     * Edit Employee Form
     * @param {Employee} employee
     * @param template
     */
    public showEditEmployeeForm(employee: Employee, template: any) {
        this.employee = employee;

        this.employeeForm.setValue({
            _id: employee._id,
            name: employee.name,
            surname: employee.surname,
            dateOfBirth: moment(employee.dateOfBirth, 'DD.MM.YYYY').toDate(),
            jobTitle: employee.jobTitle,
        });

        this.modalRef = this.modalService.show(template);
    }

    /**
     * @param {TemplateRef<any>} template
     */
    public openModal(template: TemplateRef<any>) {
        this.createForm();
        this.modalRef = this.modalService.show(template);
    }

    public closeFirstModal() {
        this.modalRef.hide();
        this.modalRef = null;
    }

    /**
     * @param {NgForm} employeeForm
     */
    public onSubmitEmployeeForm(employeeForm: NgForm) {
        let employee: Employee = <Employee>(employeeForm.value);

        employee.dateOfBirth = moment(new Date(employee.dateOfBirth)).format('DD.MM.YYYY');

        if (employee._id) {
            this.updateEmployee(employee);
        } else {
            this.createEmployee(employee);
        }

    }

}
