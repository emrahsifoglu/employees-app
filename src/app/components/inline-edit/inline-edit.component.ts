import { Input, Output, ElementRef, ViewChild, Component, forwardRef, EventEmitter, OnInit} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

const INLINE_EDIT_CONTROL_VALUE_ACCESSOR = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => InlineEditComponent),
    multi: true
};

@Component({
    selector: 'inline-edit',
    providers: [INLINE_EDIT_CONTROL_VALUE_ACCESSOR],
    styleUrls: ['./inline-edit.component.css'],
    templateUrl: './inline-edit.component.html'
})
export class InlineEditComponent implements ControlValueAccessor, OnInit {

    @ViewChild('inlineEditControl') inlineEditControl: ElementRef; // input DOM element
    @Output() public onSave: EventEmitter<any> = new EventEmitter();
    @Input() label: string = '';  // Label value for input element
    @Input() type: string = 'text'; // The type of input element
    @Input() required: boolean = false; // Is input requried?
    @Input() disabled: boolean = false; // Is input disabled?
    private _value: string = ''; // Private variable for input value
    private preValue: string = ''; // The value before clicking to edit
    private editing: boolean = false; // Is Component in edit mode?
    public onChange: any = Function.prototype; // Trascend the onChange event
    public onTouched: any = Function.prototype; // Trascend the onTouch event

    // get accessor
    get value(): any { return this._value; };

    // set accessor including the onChange callback
    set value(v: any) {
        if (v !== this._value) {
            this._value = v;
            this.onChange(v);
        }
    }

    constructor() {

    }

    ngOnInit() {

    }

    // Will update the internal data model with incoming values
    writeValue(value: any) {
        this._value = value;
    }

    // ControlValueAccessor interface
    public registerOnChange(fn: (_: any) => { }): void {
        this.onChange = fn;
    }

    // ControlValueAccessor interface
    public registerOnTouched(fn: () => { }): void {
        this.onTouched = fn;
    };

    // Do stuff when the input element loses focus
    onBlur($event: Event) {
        this.editing = false;
    }

    // Start the editting process for the input element
    edit(value: any) {
        if (this.disabled) {
            return;
        }

        this.preValue = value;
        this.editing = true;

        setTimeout((_: any) => this.inlineEditControl.nativeElement.focus(), 'focus', []);
    }

    // Method to display the editable value as text and emit save event to host
    onSubmit(value: any) {
        this.onSave.emit(value);
        this.editing = false;
    }

    // Method to reset the editable value
    cancel() {
        this._value = this.preValue;
        this.editing = false;
    }

}
