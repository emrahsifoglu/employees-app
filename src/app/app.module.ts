import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GraphQLModule } from './graphql.module';
import { EmployeeGraphQLService } from './services/employee/employee-graphql.service';
import { ShowErrorsComponent } from './components/show-errors/show-errors.component';
import { DateValidatorDirective } from './validators/date-validator.directive';
import { InlineEditComponent } from './components/inline-edit/inline-edit.component';
import { EmployeesListComponent } from './components/employees-list/employees-list.component';
import { NKDatetimeModule } from 'ng2-datetime/ng2-datetime';
import { AlertService } from './services/alert.service';
import { AlertComponent } from './components/alert/alert.component';

@NgModule({
    declarations: [
        AppComponent,
        AlertComponent,
        ShowErrorsComponent,
        InlineEditComponent,
        EmployeesListComponent,
        DateValidatorDirective,
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        GraphQLModule,
        FormsModule,
        ReactiveFormsModule,
        NKDatetimeModule,
        BsDropdownModule.forRoot(),
        TooltipModule.forRoot(),
        ModalModule.forRoot(),
    ],
    exports: [BsDropdownModule, TooltipModule, ModalModule],
    providers: [EmployeeGraphQLService, AlertService],
    bootstrap: [AppComponent]
})
export class AppModule { }
