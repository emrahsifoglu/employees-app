## Employees App

Create an application with the list of the employees using any javascript framework/library (eg. React, AngularJS, Angular 2, Vue). Angular 2 framework is preferred.

1. **Each line/row will contain one employee and the following information:**
    1. Name
    2. Surname
    3 Job title (select from the predefined list of values from API using GET method : http://ibillboard.com/api/positions)
    4. Date of birth
2. **Page should provide the following functions:**
    1. Adding a new employee
    2. Removing/deleting employees
    3. Inline editing of values
3. **When entering / editing, these values must be validated:**
    1. Name, Surname and date of birth are required
    2. What is important for us:
    3. Unit tests
4. **Application architecture**
    1. Clean code
    2. Easy deployment of the app in a production environment
    3. Do not forget about the reusability of the code.

## Resources
- https://blog.cloudboost.io/a-crud-app-with-apollo-graphql-nodejs-express-mongodb-angular5-2874111cd6a5
- https://templecoding.com/blog/2016/02/02/how-to-setup-testing-using-typescript-mocha-chai-sinon-karma-and-webpack/
- https://www.radzen.com/blog/testing-angular-webpack-mocha/
- https://hichambi.github.io/2016/12/27/testing-angular2-with-webpack-mocha-on-browser-and-node.html
- https://damienbod.com/2017/02/01/hot-module-replacement-with-angular-2-and-webpack/
- https://medium.com/developing-an-angular-4-web-app/setting-up-our-angular-4-project-from-scratch-bdbc616f92f2
- https://github.com/kashishgupta1990/simple-angular-server
- https://github.com/loiane/angular-bootstrap-example
- https://github.com/Bigless27/Angular-webpack-starter
- https://github.com/qontu/ngx-inline-editor
- https://stackoverflow.com/questions/43557321/angular-4-how-to-include-bootstrap/43557370
- https://stackoverflow.com/questions/39313095/angular2-if-ngmodel-is-used-within-a-form-tag-either-the-name-attribute-must-be
- https://medium.com/front-end-hacking/inline-editing-with-angular2-58b43cc2aba
- https://www.npmjs.com/package/ng2-inline-editor
- https://stackoverflow.com/questions/38786995/avoid-angular2-to-systematically-submit-form-on-button-click
- https://stackoverflow.com/questions/36469710/angular-2-ngmodel-bind-in-nested-ngfor
- https://www.codementor.io/godson.ukpere/creating-an-inline-edit-component-in-angular-2-nmkdlpxtq
- http://javasampleapproach.com/spring-framework/spring-mvc/angular-4-spring-boot-cassandra-crud-example
- https://angularfirebase.com/lessons/sharing-data-between-angular-components-four-methods/
- https://stackoverflow.com/questions/39647407/ng2-datetime-typeerror-datepicker-is-not-a-function
- https://stackoverflow.com/questions/37732/what-is-the-regex-pattern-for-datetime-2008-09-01-123545
- https://stackoverflow.com/questions/15491894/regex-to-validate-date-format-dd-mm-yyyy
- https://medium.com/@hnmtechshack/working-with-moment-js-8393b6dccfcb
- https://embed.plnkr.co/vWBT2nWmtsXff0MXMKdd/
- https://github.com/mgmarlow/Creating-Interfaces-Angular-Services
